import {
  SET_LOGGER_USERNAME,
  SET_EMAIL,
  SET_PASSWORD,
  SET_REPASSWORD,
  SET_REGISTER,
  SET_ERROR,
} from '../actions/Types';

const initialState = {
  username: '',
  email: '',
  password: '',
  repassword: '',
  error: false,
  register: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGGER_USERNAME:
      return {
        ...state,
        username: action.payload,
      };

    case SET_EMAIL:
      return {
        ...state,
        email: action.payload,
      };
    case SET_PASSWORD:
      return {
        ...state,
        password: action.payload,
      };
    case SET_REPASSWORD:
      return {
        ...state,
        repassword: action.payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case SET_REGISTER:
      return {
        ...state,
        register: action.payload,
      };

    default:
      return state;
  }
};
