import {combineReducers} from 'redux';
import settingsReducer from './Settings';
import loggerReducer from './Logger';

const rootReducer = combineReducers({
  logger: loggerReducer,
  settings: settingsReducer,
});

export default rootReducer;
