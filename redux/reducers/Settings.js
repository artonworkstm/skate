import {SET_BACKGROUND, SET_USERNAME} from '../actions/Types';

const initialState = {
  backgroundColor: '#111',
  username: 'User',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_BACKGROUND:
      return {
        ...state,
        backgroundColor: action.payload,
      };
    case SET_USERNAME:
      return {
        ...state,
        username: action.payload,
      };

    default:
      return state;
  }
};
