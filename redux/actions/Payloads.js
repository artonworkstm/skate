import {
  SET_BACKGROUND,
  SET_USERNAME,
  SET_LOGGER_USERNAME,
  SET_EMAIL,
  SET_PASSWORD,
  SET_REPASSWORD,
  SET_ERROR,
  SET_REGISTER,
} from './Types';

export const setBackground = (value) => ({
  type: SET_BACKGROUND,
  payload: value,
});

export const setUsername = (value) => ({
  type: SET_USERNAME,
  payload: value,
});

export const setLoggerUsername = (value) => ({
  type: SET_LOGGER_USERNAME,
  payload: value,
});

export const setEmail = (value) => ({
  type: SET_EMAIL,
  payload: value,
});

export const setPassword = (value) => ({
  type: SET_PASSWORD,
  payload: value,
});

export const setRePassword = (value) => ({
  type: SET_REPASSWORD,
  payload: value,
});

export const setError = (value) => ({
  type: SET_ERROR,
  payload: value,
});

export const setRegister = (value) => ({
  type: SET_REGISTER,
  payload: value,
});
