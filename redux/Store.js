import {createStore} from 'redux';
import rootReducer from './reducers/CombineReducers';

const initialState = {};
const store = createStore(rootReducer, initialState);

export default store;
