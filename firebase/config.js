import firebase from '@react-native-firebase/app';

const firebaseConfig = {
  apiKey: 'AIzaSyD_jJsOxjD_NBHkfZXbCU-z5Q3pYRFa1TI',
  authDomain: 'skate-the-game.firebaseapp.com',
  databaseURL: 'https://skate-the-game.firebaseio.com',
  projectId: 'skate-the-game',
  storageBucket: 'skate-the-game.appspot.com',
  messagingSenderId: '632237123439',
  appId: '1:632237123439:web:4c73dd1cd188e2a6c52a9a',
  measurementId: 'G-DQ8B06EE59',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export {firebaseConfig};
