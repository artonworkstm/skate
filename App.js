import React, {useState} from 'react';
import {Provider} from 'react-redux';
import {firebase} from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';

import store from './redux/Store';
import Navigation from './components/Navigation/Navigation';

const App = () => {
  const [user, setUser] = useState(null);
  firebase.apps.length && auth().onAuthStateChanged((user) => setUser(user));
  return (
    <Provider store={store}>
      <Navigation user={user} />
    </Provider>
  );
};

export default App;
