//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
  setLoggerUsername,
  setEmail,
  setPassword,
  setRePassword,
  setError,
  setRegister,
} from '../../redux/actions/Payloads';

// create a component
const Signup = ({onSignup, onRedirect}) => {
  return (
    <>
      <View style={styles.inputContainer}>
        <Input
          placeholder="Username"
          placeholderTextColor="#e2e2e2"
          leftIcon={<Icon name="person" color="#c9c9c9" size={28} />}
          style={styles.input}
          onChange={(e) => onUsername(e.nativeEvent.text)}
        />
        <Input
          placeholder="E-mail"
          textContentType="emailAddress"
          placeholderTextColor="#e2e2e2"
          leftIcon={<Icon name="mail" color="#c9c9c9" size={28} />}
          style={styles.input}
          onChange={(e) => onEmail(e.nativeEvent.text)}
        />
        <Input
          placeholder="Password"
          textContentType="newPassword"
          placeholderTextColor="#e2e2e2"
          leftIcon={<Icon name="lock-closed" color="#c9c9c9" size={28} />}
          style={styles.input}
          onChange={(e) => onPassword(e.nativeEvent.text)}
        />
        <Input
          placeholder="Password again"
          textContentType="password"
          placeholderTextColor="#e2e2e2"
          leftIcon={<Icon name="lock-closed" color="#c9c9c9" size={28} />}
          style={styles.input}
          onChange={(e) => onRePassword(e.nativeEvent.text)}
        />
      </View>
      <View style={styles.buttonContainer}>
        <Button title="Sign up" onPress={onSignup} />
      </View>
      <View style={styles.button}>
        <Button
          title="Already have an ccount ? Sign in!"
          type="clear"
          onPress={onRedirect}
        />
      </View>
    </>
  );
};

// define your styles
const styles = StyleSheet.create({
  inputContainer: {
    width: '80%',
  },
  input: {
    color: '#eee',
  },
  buttonContainer: {
    marginTop: '10%',
    width: '80%',
  },
  button: {
    position: 'absolute',
    bottom: 20,
  },
});

//make this component available to the app
export default Signup;
