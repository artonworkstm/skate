//import liraries
import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useEffect} from 'react';

// create a component
export const Header = ({
  color,
  toggle,
  handleLogout,
  handleToggle,
  handleRedirect,
}) => {
  return (
    <View
      style={[
        styles.container,
        {
          height: toggle ? '40%' : '10%',
          paddingVertical: toggle ? 0 : 30,
          position: toggle ? 'relative' : 'absolute',
        },
      ]}>
      <View
        style={[
          styles.toggleContainer,
          {height: toggle ? '10%' : '100%', marginVertical: toggle ? 30 : 0},
        ]}>
        <TouchableOpacity onPress={handleToggle}>
          <Icon name="ellipsis-horizontal" size={38} color={color} />
        </TouchableOpacity>
        <TouchableOpacity onPress={handleLogout}>
          <Icon
            name="log-out"
            style={{display: toggle ? 'flex' : 'none'}}
            size={38}
            color={'#821'}
          />
        </TouchableOpacity>
      </View>
      <View
        style={[
          styles.unvisibleContainer,
          {display: toggle ? 'flex' : 'none'},
        ]}>
        <View style={styles.menuItem}>
          <TouchableOpacity onPress={() => handleRedirect('Settings')}>
            <Text style={styles.itemFont}>Settings</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.menuItem}>
          <Text style={styles.itemFont}>Discover</Text>
        </View>
        <View style={styles.menuItem}>
          <Text style={styles.itemFont}>S.K.A.T.E.</Text>
        </View>
        <View style={styles.menuItem}>
          <Text style={styles.itemFont}>...</Text>
        </View>
      </View>
    </View>
  );
};

export const HeaderSub = ({color, handlePrevious}) => {
  return (
    <View style={[styles.container, {paddingVertical: 30}]}>
      <View style={styles.toggleContainer}>
        <TouchableOpacity onPress={handlePrevious}>
          <Icon name="chevron-back" size={38} color={color} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'column',
    paddingHorizontal: '7%',
  },
  toggleContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  unvisibleContainer: {
    height: '75%',
    width: '100%',
  },
  menuItem: {
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  itemFont: {
    fontSize: 38,
    color: '#fff',
  },
});
