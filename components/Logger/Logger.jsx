//import liraries
import React from 'react';
import {View, StyleSheet} from 'react-native';
import auth from '@react-native-firebase/auth';
import {useSelector, useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
  setError,
  setUsername,
  setRegister,
  setBackground,
} from '../../redux/actions/Payloads';
import Signin from '../Signin/Signin';
import Signup from '../Signup/Signup';

// create a component
const Logger = ({navigation, route}) => {
  const actions = bindActionCreators(
    {
      setError,
      setUsername,
      setRegister,
      setBackground,
    },
    useDispatch(),
  );

  /** get State from Store */
  const {
    settings: {backgroundColor},
    logger: {username, email, password, repassword, error, register},
  } = useSelector((state) => state);

  const onLogin = () => {
    if (email !== '' && password !== '')
      auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          const username = auth().currentUser.displayName;
          actions.setUsername(username);
          navigation.navigate('Home');
        })
        .catch((err) => actions.setError(err));
  };

  const onRegister = () => {
    String(password) === String(repassword) &&
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          auth().currentUser.updateProfile({
            displayName: username,
          });
          actions.setUsername(username);
          navigation.navigate('Home');
        })
        .catch((err) => actions.setError(err));
  };

  const handleRedirect = () => {
    actions.setRegister(!register);
  };

  return (
    <View style={[styles.container, {backgroundColor: backgroundColor}]}>
      {!register ? (
        <Signin error={error} onRedirect={handleRedirect} onSignin={onLogin} />
      ) : (
        <Signup onRedirect={handleRedirect} onSignup={onRegister} />
      )}
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

//make this component available to the app
export default Logger;
