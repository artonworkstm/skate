//import liraries
import React, {useEffect} from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import {firebase} from '../../firebase/config';

// create a component
const Loading = ({navigation, route, user}) => {
  useEffect(() => {
    //console.log(user);
    user ? navigation.navigate('Home') : navigation.navigate('Logger');
  });

  return (
    <View style={styles.container}>
      <ActivityIndicator size="large" color="#111" />
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
});

//make this component available to the app
export default Loading;
