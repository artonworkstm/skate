//import liraries
import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
  setLoggerUsername,
  setEmail,
  setPassword,
  setError,
  setRegister,
} from '../../redux/actions/Payloads';

// create a component
const Signin = ({error, onSignin, onRedirect}) => {
  const actions = bindActionCreators(
    {
      setLoggerUsername,
      setEmail,
      setPassword,
      setError,
      setRegister,
    },
    useDispatch(),
  );

  return (
    <>
      <View style={styles.inputContainer}>
        <Input
          placeholder="E-mail"
          errorMessage={error === false ? '' : String(error)}
          textContentType="emailAddress"
          placeholderTextColor="#e2e2e2"
          leftIcon={<Icon name="mail" color="#c9c9c9" size={28} />}
          inputStyle={styles.input}
          errorStyle={styles.error}
          onChange={(e) => {
            actions.setEmail(e.nativeEvent.text);
          }}
          renderErrorMessage={error === false ? false : true}
        />
        <Input
          secureTextEntry={true}
          placeholder="Password"
          textContentType="password"
          placeholderTextColor="#e2e2e2"
          leftIcon={<Icon name="lock-closed" color="#c9c9c9" size={28} />}
          inputStyle={styles.input}
          onChange={(e) => actions.setPassword(e.nativeEvent.text)}
        />
      </View>
      <View style={styles.buttonContainer}>
        <Button title="Sign in" onPress={onSignin} />
      </View>
      <View style={styles.button}>
        <Button
          title="Don't have an ccount ? Create one!"
          type="clear"
          onPress={onRedirect}
        />
      </View>
    </>
  );
};

// define your styles
const styles = StyleSheet.create({
  inputContainer: {
    width: '80%',
  },
  input: {
    color: '#eee',
  },
  error: {
    color: '#800',
  },
  buttonContainer: {
    marginTop: '10%',
    width: '80%',
  },
  button: {
    position: 'absolute',
    bottom: 20,
  },
});

//make this component available to the app
export default Signin;
