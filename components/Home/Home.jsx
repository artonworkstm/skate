//import liraries
import React, {useLayoutEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux';

import {setBackground, setUsername} from '../../redux/actions/Payloads';
import {Header} from '../Header/Header';
import auth from '@react-native-firebase/auth';
import {Button} from 'react-native-elements';
import {useState} from 'react';

// create a component
const Home = ({navigation, route}) => {
  const [toggle, setToggle] = useState(false);
  /** get action Payloads */
  const actions = bindActionCreators(
    {setBackground, setUsername},
    useDispatch(),
  );

  /** get State from Store */
  const {
    settings: {backgroundColor, username},
  } = useSelector((state) => state);

  const handleRedirect = (screen) => {
    navigation.navigate(screen);
  };

  const handleToggle = () => {
    setToggle(!toggle);
  };
  const handleLogout = () => {
    auth().signOut();
  };

  return (
    <View style={[styles.container, {backgroundColor: backgroundColor}]}>
      <Header
        color={backgroundColor === '#fff' ? '#111' : '#eee'}
        toggle={toggle}
        handleToggle={handleToggle}
        handleLogout={handleLogout}
        handleRedirect={handleRedirect}
      />
      <Text
        style={[
          styles.welcome,
          {color: backgroundColor === '#fff' ? '#111' : '#eee'},
        ]}>
        Hello {username}!
      </Text>
      <View>
        <Button
          title="Sign out"
          color="#800"
          onPress={() => auth().signOut()}
        />
      </View>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#2c3e50',
    //paddingTop: '10%' /** Due to missing Profile pic */,
  },
  welcome: {
    fontSize: 38,
  },
});

//make this component available to the app
export default Home;
