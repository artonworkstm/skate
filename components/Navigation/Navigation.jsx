//import liraries
import React from 'react';
import {StyleSheet, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';

import Home from '../Home/Home';
import Settings from '../Settings/Settings';
import Loading from '../Loading/Loading';
import Logger from '../Logger/Logger';

const Stack = createStackNavigator();
const topBarHeight = StatusBar.currentHeight;

// create a component
const Navigation = ({user}) => {
  /** get State from Store */
  const {
    settings: {backgroundColor},
  } = useSelector((state) => state);

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          header: () => null,
        }}>
        <Stack.Screen name="Loading">
          {(props) => <Loading {...props} user={user} />}
        </Stack.Screen>
        <Stack.Screen name="Home">
          {(props) => <Home {...props} />}
        </Stack.Screen>
        <Stack.Screen name="Settings">
          {(props) => <Settings {...props} />}
        </Stack.Screen>
        <Stack.Screen name="Logger">
          {(props) => <Logger {...props} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  titleFont: {
    fontSize: 24,
  },
});

//make this component available to the app
export default Navigation;
