//import liraries
import React from 'react';
import {View, Text, TextInput, StyleSheet, Switch} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux';

import {setBackground, setUsername} from '../../redux/actions/Payloads';
import {HeaderSub} from '../Header/Header';

// create a component
const Settings = ({navigation, route}) => {
  /** get action Payloads */
  const actions = bindActionCreators(
    {setBackground, setUsername},
    useDispatch(),
  );

  /** get State from Store */
  const {
    settings: {backgroundColor, username},
  } = useSelector((state) => state);

  const handleUsernameChange = (e) => {
    actions.setUsername(e.nativeEvent.text);
  };

  const handleToggleSwitch = () => {
    actions.setBackground(backgroundColor === '#fff' ? '#111' : '#fff');
  };

  const handlePrevious = () => {
    navigation.goBack();
  };

  return (
    <View style={[styles.container, {backgroundColor: backgroundColor}]}>
      <HeaderSub
        color={backgroundColor === '#fff' ? '#111' : '#eee'}
        handlePrevious={handlePrevious}
      />
      <View style={styles.inputContainer}>
        <Text
          style={[
            styles.title,
            {color: backgroundColor === '#fff' ? '#111' : '#eee'},
          ]}>
          Username:
        </Text>
        <TextInput
          value={username}
          onChange={handleUsernameChange}
          style={[
            styles.usernameInput,
            {color: backgroundColor === '#fff' ? '#111' : '#eee'},
          ]}
        />
      </View>
      <View style={styles.inputContainer}>
        <Text
          style={[
            styles.title,
            {color: backgroundColor === '#fff' ? '#111' : '#eee'},
          ]}>
          Dark mode
        </Text>
        <Switch
          trackColor={{false: '#767577', true: '#81b0ff'}}
          thumbColor={backgroundColor === '#fff' ? '#f5dd4b' : '#f4f3f4'}
          ios_backgroundColor="#3e3e3e"
          onValueChange={handleToggleSwitch}
          value={backgroundColor === '#fff' ? false : true}
        />
      </View>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
  inputContainer: {
    marginTop: '20%',
    alignItems: 'center',
  },
  title: {
    fontSize: 34,
    textDecorationLine: 'underline',
    marginBottom: 20,
  },
  usernameInput: {
    fontSize: 28,
  },
});

//make this component available to the app
export default Settings;
